import re
import json


# load the stopwords
with open("stopwords.json") as handle:
    stopwords = set(json.load(handle))

# a more comprhensive list of stopwords
with open("long_stopwords.json") as handle:
    long_stopwords = stopwords.union(set(json.load(handle)))

# load the valid 1-char words
with open("1words.json") as handle:
    chars1 = json.load(handle)

# load the valid 2-char words
with open("2words.json") as handle:
    chars2 = json.load(handle)

# How to split the line. (\W means all non-characters)
IN_CONTEX_SPLITTING = "\W"


def check_valid_word(word, stopwords, chars1, chars2):
    """
    Check if a word is valid. It ignore words in the stopword list.
    Words long 1 or 2 characters are valid only if present in the chars1 or chars2 list.

    :param word: word to check
    :param stopwords: list of stopwords
    :param chars1: list of valid words long 1 character
    :param chars2: lisrt of valid words long 2 characters
    :return: True if the word is valid
    """
    if word.isdigit():
        return True
    if (not word or
            word in stopwords or
            (len(word) == 1 and word not in chars1) or
            (len(word) == 2 and word not in chars2)):
        return False
    return True


def tokenize_line(line, pattern=IN_CONTEX_SPLITTING, use_long_stopwords=False):
    """
    Normalizes into lowers case and splits a sentence into tokens, retaining only valid words.

    :param line: the line to be splitted
    :param pattern: the pattern that delimits each word
    :param use_long_stopwords: use a long stopwords list or not
    :return: a list of tokens (word)
    """
    return [word for word in re.split(pattern, line.lower())
            if check_valid_word(word,
                                long_stopwords if use_long_stopwords else stopwords,
                                chars1,
                                chars2)]




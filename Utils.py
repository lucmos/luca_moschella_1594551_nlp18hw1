import os
import pickle
import json


def load_pickle(filename):
    """
    Loads a given pickle file
    :param filename: the pickle file name
    :return: the loaded data
    """
    if os.path.isfile(filename):
        with open(filename, 'rb') as handle:
            return pickle.load(handle)
    return False


def save_pickle(obj, filename):
    """
    Save a object to a pickle file with the highest protocol available
    :param obj: object to save
    :param filename: pickle file name
    """
    if os.path.isfile(filename):
        filename = "avoid_overwriting_" + filename  # to not lose info for distraction

    with open(filename, 'wb') as handle:
        pickle.dump(obj, handle, protocol=pickle.HIGHEST_PROTOCOL)


def load_json(filename):
    """
        Loads a given json file
        :param filename: the json file name
        :return: the loaded data
    """
    if os.path.isfile(filename):
        with open(filename, 'r') as handle:
            return json.load(handle)
    return False


def save_json(obj, filename):
    """
    Save a object to a json file
    :param obj: object to save
    :param filename: json file name
    """
    if os.path.isfile(filename):
        filename = "avoid_overwriting_" + filename  # to not lose info for distraction

    with open(filename, "w") as handle:
        json.dump(obj, handle, indent=4, sort_keys=True)


def min_max_normalization(array):
    """
    Performs a min-max normalization on the array
    :param array: the array to normalize
    :return: the normalized array
    """
    minx = min(array)
    maxx = max(array)
    return [(x - minx) / (maxx - minx) for x in array]

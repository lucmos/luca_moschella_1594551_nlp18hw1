import os

import tensorflow as tf
import numpy as np
import tqdm
import scipy.stats
import math
import time
import random as rand

from tensorboard.plugins import projector

import tokenizer
import Utils
from data_preprocessing import generate_batch, build_dataset, save_vectors, read_analogies
from evaluation import evaluation
from Chronometer import Chrono

# run on CPU
# comment this part if you want to run it on GPU
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"  # see issue #152
os.environ["CUDA_VISIBLE_DEVICES"] = ""

# ----------------------- PARAMETERS ----------------------- #
NUM_STEPS = 37000000        # Number of steps to perform.
BATCH_SIZE = 512            # Number of samples per batch
WINDOW_SIZE = 10            # How many words to consider left and right.

EMBEDDING_SIZE = 128        # Dimension of the embedding vector.

VOCABULARY_SIZE = 100000    # The most N word to consider in the dictionary
DOMAIN_WORDS = None         # Dimension of each domain, None = all train data

NEG_SAMPLES = 64            # Number of negative examples to sample.
LEARNING_RATE = 1           # Learning rate of adagrad

SUMMARY_FREQUENCY = 100000  # After how many steps save loss and accuracy for summary

# The array represents a  min-max normalized Normal Distribution.
# The Gaussian is centered in the center of the array, with stand. dev. equal to WINDOW_SIZE/2.
# This array will be used to randomize following a normal distribution the generation of the couples (word, label).
# The main intuition is that closer words are more probably correlated, but:
# # # # Some very close word may not be!
# # # # Even distant words some times may be!
# Other than this, randomization partially solves the problem of overfitting.
#
# With a window size equal to 10, the left-side of the array is the following (it's simmetric):
# [0.0, 0.05, 0.1, 0.17, 0.24, 0.32, 0.41, 0.5, 0.59, 0.68, 0.77, 0.85, 0.91, 0.96, 0.99] center word [right-side]
# The expected number of pairs that will be generated is: 15
WINDOW_PROBABILITY_DISTRIBUTION = Utils.min_max_normalization([scipy.stats.norm(0, WINDOW_SIZE / 2).pdf(x)
                                                               for x in np.arange(-WINDOW_SIZE, WINDOW_SIZE + 1, 1)])
# ---------------------------------------------------------- #


# ------------------------- PATHS ------------------------ #
TRAIN_DIR = "dataset/DATA/TRAIN"
VALID_DIR = "dataset/DATA/DEV"
TMP_DIR = "/media/luca/Storage/tmp"
ANALOGIES_FILE = "dataset/eval/questions-words.txt"

# chaching for the dataset
RAW_DATA_PICKLE = "dataset_raw.pickle"

timestr = time.strftime("%d-%m-%Y_%Hh-%Mm")
EMBEDDING_OUTPUT_FILE = "embeddings_{}.pickle".format(timestr)
SUMMARY_OUTPUT_JSON = "summary_{}.json".format(timestr)
SUMMARY_OUTPUT_PICKLE = "summary_{}.pickle".format(timestr)
print(timestr)
# -------------------------------------------------------- #

# summary to save for later analysis
results_summary = {
    "execution_id": timestr,

    "num_steps": NUM_STEPS,
    "batch_size": BATCH_SIZE,
    "window_size": WINDOW_SIZE,

    "embedding_size": EMBEDDING_SIZE,

    "vocabulary_size": VOCABULARY_SIZE,
    "domain_words": DOMAIN_WORDS,

    "loss_fun": "nce",                  #todo: remember to change if changed!
    "negative_samples": NEG_SAMPLES,
    "optimizer_fun": "adagrad",         #todo: remember to change if changed!
    "learning_rate": LEARNING_RATE,

    "window_distribution:": WINDOW_PROBABILITY_DISTRIBUTION,

    "summary_freq": SUMMARY_FREQUENCY,
    "step_avgloss_correct_total_accuracy": [],

    "elapsed_millis": None
}


RANDOM_SEED = 42  # just a random seed to shuffle
def read_data(directory, domain_words=None):
    """
    Reads the data in the trainig set, then shuffles it.
    The data is NOT read like a single list of words, it tries to put in a list only words that probably are correlated.

    If assumes that:
    1) Two documents contain unrelated contents
    2) A change of context in the same document occurs only when a new paragraph begins
    3) Two paragraphs are separated by at least a "\n"

    So the read data are represented by a list of list of words.

    :param directory: the train data directory
    :param domain_words: how many word to load from each domain. None if all data must be loaded.
    :return: a (shuffled) list of lists of words.
    """
    # If there is a chached version, use it
    cached_pickle = Utils.load_pickle(RAW_DATA_PICKLE)
    if cached_pickle:
        return cached_pickle

    data = []
    for domain in os.listdir(directory):
        words_in_domain = 0
        for f in os.listdir(os.path.join(directory, domain)):
            if f.endswith(".txt"):
                with open(os.path.join(directory, domain, f)) as file:
                    for line in file.readlines():

                        #  let out stopwords and words composed of a single character
                        split = tokenizer.tokenize_line(line)
                        if split:  # let out lines composed only of special chars
                            data.append(split)
                        words_in_domain += len(split)

                        if domain_words and words_in_domain > domain_words:
                            break
            if domain_words and words_in_domain > domain_words:
                break

    rand.seed(RANDOM_SEED)
    rand.shuffle(data)

    # Save the file in a temp file, to reuse it faster.
    Utils.save_pickle(data, RAW_DATA_PICKLE)

    return data


# load the training set, a cached one if there is
raw_data = read_data(TRAIN_DIR, domain_words=DOMAIN_WORDS)

# print some information about the corpus
print('Number of paragraphs:', len(raw_data))
print('Number of words', sum(len(x) for x in raw_data))

# the portion of the training set used for data evaluation
valid_size = 16  # Random set of words to evaluate similarity on.
valid_window = 100  # Only pick dev samples in the head of the distribution.
valid_examples = np.random.choice(valid_window, valid_size, replace=False)

# CREATE THE DATASET AND WORD-INT MAPPING ###
data, dictionary, reverse_dictionary = build_dataset(raw_data, VOCABULARY_SIZE)
del raw_data  # Hint to reduce memory.

# read the question file for the Analogical Reasoning evaluation
questions = read_analogies(ANALOGIES_FILE, dictionary)

# --------------------- MODEL DEFINITION --------------------- #

graph = tf.Graph()
eval = None

with graph.as_default():
    # Define input data tensors.
    with tf.name_scope("inputs"):
        train_inputs = tf.placeholder(tf.int32, shape=[BATCH_SIZE])
        train_labels = tf.placeholder(tf.int32, shape=[BATCH_SIZE, 1])
        valid_dataset = tf.constant(valid_examples, dtype=tf.int32)

    # define tha weight matrix between the input layer and the hidden one
    with tf.name_scope("embeddings"):
        embeddings = tf.Variable(
            tf.random_uniform([VOCABULARY_SIZE, EMBEDDING_SIZE], -1.0, 1.0))  # placeholder variable

    # define how the hidden layer is computed
    with tf.name_scope("hiddel_layer"):
        single_embedding = tf.nn.embedding_lookup(embeddings, train_inputs)

    # define the weight matrix between the hidden layer and the output, and the biases
    with tf.name_scope("matrix_hidden-output"):
        weights = tf.Variable(
            tf.truncated_normal([VOCABULARY_SIZE, EMBEDDING_SIZE], stddev=1.0 / math.sqrt(EMBEDDING_SIZE)))
        biases = tf.Variable(tf.zeros([VOCABULARY_SIZE]))

    # define the loss function
    with tf.name_scope("loss"):
        loss = tf.reduce_mean(
            tf.nn.nce_loss(
                weights=weights,
                biases=biases,
                labels=train_labels,
                inputs=single_embedding,
                num_sampled=NEG_SAMPLES,
                num_classes=VOCABULARY_SIZE))
        # loss = tf.reduce_mean(tf.nn.sampled_softmax_loss(weights=weights, biases=biases, inputs=hidden_representation,
        #                            labels=train_labels, num_sampled=NEG_SAMPLES, num_classes=VOCABULARY_SIZE))
        # loss = tf.losses.sparse_softmax_cross_entropy(labels=train_labels, logits=output) ### FILL HERE ###

    # Add the loss value as a scalar to summary.
    tf.summary.scalar('loss', loss)

    # define the optimizer
    with tf.name_scope('optimizer'):
        # optimizer = tf.train.AdamOptimizer(LEARNING_RATE).minimize(loss)
        # optimizer = tf.train.GradientDescentOptimizer(0.5).minimize(loss)
        optimizer = tf.train.AdagradOptimizer(LEARNING_RATE).minimize(loss)  # empirically the best one
        # optimizer = tf.train.AdadeltaOptimizer(LEARNING_RATE).minimize(loss)
        # optimizer = tf.train.RMSPropOptimizer(LEARNING_RATE).minimize(loss)
        # optimizer = tf.train.MomentumOptimizer(LEARNING_RATE, momentum=MOMENTUM).minimize(loss)

    # Compute the cosine similarity between minibatch examples and all embeddings.
    norm = tf.sqrt(tf.reduce_sum(tf.square(embeddings), 1, keepdims=True))
    normalized_embeddings = embeddings / norm
    valid_embeddings = tf.nn.embedding_lookup(normalized_embeddings, valid_dataset)
    similarity = tf.matmul(valid_embeddings, normalized_embeddings, transpose_b=True)

    # Merge all summaries.
    merged = tf.summary.merge_all()

    # Add variable initializer.
    init = tf.global_variables_initializer()

    # Create a saver.
    saver = tf.train.Saver()

    # evaluation graph
    eval = evaluation(normalized_embeddings, dictionary, questions)

# --------------------------- TRAINING -------------------------- #

# remember some indexes to start the next batch where I left
curr_sentence = 0
curr_word = 0
curr_label = 0

with tf.Session(graph=graph) as session:
    # Open a writer to write summaries.
    writer = tf.summary.FileWriter(TMP_DIR, session.graph)

    # We must initialize all variables before we use them.
    session.run(init)

    # just a timer
    chrono = Chrono("Initilized...", new_line=True)

    average_loss = 0
    bar = tqdm.trange(NUM_STEPS)
    for step in bar:

        batch_inputs, batch_labels, curr_sentence, curr_word, curr_label = generate_batch(
            BATCH_SIZE, curr_sentence, curr_word, curr_label, WINDOW_SIZE, WINDOW_PROBABILITY_DISTRIBUTION, data)

        # Define metadata variable.
        run_metadata = tf.RunMetadata()

        # We perform one update step by evaluating the optimizer op
        _, summary, loss_val = session.run(
            [optimizer, merged, loss],
            feed_dict={train_inputs: batch_inputs, train_labels: batch_labels},
            run_metadata=run_metadata)
        average_loss += loss_val

        # Add returned summaries to writer in each step.
        writer.add_summary(summary, step)

        # Add metadata to visualize the graph for the last run.
        if step == (NUM_STEPS - 1):
            writer.add_run_metadata(run_metadata, 'step%d' % step)

        if step == (NUM_STEPS - 1) or step % SUMMARY_FREQUENCY == 0:
            sim = similarity.eval()
            for i in range(valid_size):
                valid_word = reverse_dictionary[valid_examples[i]]
                top_k = 8  # number of nearest neighbors
                nearest = (-sim[i, :]).argsort()[1:top_k + 1]
                log_str = 'Nearest to %s:' % valid_word
                for k in range(top_k):
                    close_word = reverse_dictionary[nearest[k]]
                    log_str = '%s %s,' % (log_str, close_word)
                print(log_str)

            print("avg loss: " + str(average_loss / step))
            correct, total, accuracy = eval.eval(session)
            loss_media = average_loss / step
            results_summary["step_avgloss_correct_total_accuracy"].append((step, loss_media, correct, total, accuracy))

    final_embeddings = normalized_embeddings.eval()

    # ------------------------ SAVE THINGS --------------------- #
    elapsed_time = chrono.millis()
    results_summary["elapsed_millis"] = elapsed_time

    save_vectors(final_embeddings, reverse_dictionary, EMBEDDING_OUTPUT_FILE)
    Utils.save_json(results_summary, SUMMARY_OUTPUT_JSON)       # for a user friendly view
    Utils.save_pickle(results_summary, SUMMARY_OUTPUT_PICKLE)   # automatic handling of NaN etc when retrieving

    # Write corresponding labels for the embeddings.
    with open(os.path.join(TMP_DIR, 'metadata.tsv'), 'w') as f:
        for i in range(VOCABULARY_SIZE):
            f.write(reverse_dictionary[i] + '\n')

    # Save the model for checkpoints
    saver.save(session, os.path.join(TMP_DIR, 'model.ckpt'))

    # Create a configuration for visualizing embeddings with the labels in TensorBoard.
    config = projector.ProjectorConfig()
    embedding_conf = config.embeddings.add()
    embedding_conf.tensor_name = embeddings.name
    embedding_conf.metadata_path = os.path.join(TMP_DIR, 'metadata.tsv')
    projector.visualize_embeddings(writer, config)

writer.close()

import os
import math
import random
from collections import Counter

import numpy
import scipy.spatial
import tqdm
import json

import sklearn.metrics
import sklearn.ensemble
import sklearn.svm
import xgboost

import Utils
import tokenizer
from Chronometer import Chrono

EMBEDDINGS_FINAL = "./output_finale/embeddings_12-04-2018_07h-15m.pickle"
EMBEDDINGS_DEFAULT = EMBEDDINGS_FINAL

TRAIN_DIR = "dataset/DATA/TRAIN"
DEV_DIR = "dataset/DATA/DEV"
TEST_DIR = "dataset/DATA/TEST"

TRAINED_MODEL = "xgboost.pickle"

DATAMANAGER_CACHE_FILE = "datamanager_domaintask_caching.pickle"
METRICS_CACHE_FILE = "metrics_domaintask_caching.pickle"

DOMAIN_IMPORTANCE_CACHE_FILE = "metrics_domain_importance.pickle"
FILE_EMBEDDINGS_CACHE = "metrics_file_embeddings.pickle"

TRAINING_DATA_CACHE = "training_data_cache.pickle"

ONLY_EMBD = "only_embed_"
ONLY_IMPCOS = "only_impcos_"
BOTH = "both"

TRAIN = "train"
DEV = "dev"
TEST = "test"

# training dictionary keys
X_TRAIN = "x_train"
Y_TRAIN = "y_train"
X_DEV = "x_dev"
Y_DEV = "y_dev"
X_TEST = "x_test"

TRAIN_FILES_IDS = "train_files_ids"
DEV_FILES_IDS = "dev_files_ids"
TEST_FILES_IDS = "test_files_ids"

TRAIN_FILENAMES = "train_filename_idfile"
DEV_FILENAMES = "dev_filename_idfile"
TEST_FILENAMES = "test_filename_idfile"

TRAIN_IDFILE_DOMAIN = "train_idfile_domain"
DEV_IDFILE_DOMAIN = "dev_idfile_domain"
TEST_IDFILE_DOMAIN = "test_idfile_domain"


class DataManager:
    """
    Tanages the loading of the dataset, the conversion with the embeddings and performs some statistics.
    """

    @staticmethod
    def get_instance(embeddings_pickle=EMBEDDINGS_DEFAULT,
                     train_dir=TRAIN_DIR, dev_dir=DEV_DIR,
                     test_dir=TEST_DIR) -> 'DataManager':
        """
        Loads an instance of the class from a pickle file, if there is a cached version.

        :param embeddings_pickle: the cache file
        :param train_dir: the train dir
        :param dev_dir: the dev dir
        :param test_dir: the test dir
        :return: an istance of Data Manager
        """
        c = Chrono("Loading the data...")
        instance = Utils.load_pickle(DATAMANAGER_CACHE_FILE)
        if not instance:
            instance = DataManager(embeddings_pickle, train_dir, dev_dir, test_dir)
            Utils.save_pickle(instance, DATAMANAGER_CACHE_FILE)
        c.millis()
        return instance

    def __init__(self, embeddings_pickle=EMBEDDINGS_DEFAULT,
                 train_dir=TRAIN_DIR, dev_dir=DEV_DIR, test_dir=TEST_DIR):
        self.embeddings_pickle = embeddings_pickle
        self.test_dir = test_dir
        self.dev_dir = dev_dir
        self.train_dir = train_dir

        self.word2vec = None

        self.name_domain2token_domain = {}
        self.list_domains = None

        # ----- EACH DICTIONARY IS DIVIDED FIRSTLY BY TRAIN, DEV OR TEST ----- #
        self.files_sentences_words = {}

        self.id_file2domain = {}

        self.files_ids = {}
        self.files_names = {}

        self.id_file2setwords = {}
        self.id_file2word2number_words = {}

        self.word2total_count = {}
        self.word2doc_frequency = {}
        # -------------------------------------------------------------------- #

        self.load_embeddings()
        self.load_train_data()
        self.load_dev_data()
        self.load_test_data()
        self.count_stats()

    def count_stats(self):
        """
        Performs some statistics on the data, subdividing it based on TRAIN, TEST, DEV.
        It computes the:
        ) For each file, the number of occurrence of each word
        ) For each file, the set of words that appear in it
        ) For each word, the total number of occurrences
        ) For each word, its document frequency (number of documents in which it appears)
        """
        for dataset in [TRAIN, DEV, TEST]:
            self.id_file2setwords[dataset] = {}
            self.id_file2word2number_words[dataset] = {}
            self.word2total_count[dataset] = Counter()
            self.word2doc_frequency[dataset] = Counter()

            bar = tqdm.tqdm(enumerate(self.files_sentences_words[dataset]))
            for i, file in bar:
                bar.set_description("Counting for dataset: {}".format(dataset))
                file_set_words = set()
                word2count_in_file = Counter()

                for sentence in file:
                    file_set_words.update(sentence)
                    for word in sentence:
                        if word in self.word2vec:
                            word2count_in_file[word] += 1
                            self.word2total_count[dataset][word] += 1

                self.id_file2setwords[dataset][i] = file_set_words
                self.id_file2word2number_words[dataset][i] = word2count_in_file

                for word in file_set_words:
                    self.word2doc_frequency[dataset][word] += 1

    def load_embeddings(self):
        """
        Loads the specified embeddings

        :return: the loaded pickle
        """
        self.word2vec = Utils.load_pickle(self.embeddings_pickle)
        # with open(embeddings_pickle, "rb") as f:
        #     word2vec = pickle.load(f)
        #
        # # matrix = []
        # # dictionary = {}
        # # reverse_dictionary = {}
        # # for i, word in enumerate(word2vec):
        # #     matrix.append(word2vec[word])
        # #     dictionary[word] = i
        # #     reverse_dictionary[i] = word
        #
        # return word2vec

    def lookup(self, word):
        """
        Return the embedding of a given word

        :param word: the given word
        :return: the embedding of the given word
        """
        return self.word2vec[word] if word in self.word2vec else self.word2vec["UNK"]

    def load_train_data(self):
        """
        Reads the train data
        """
        c = Chrono("Loading train data...", new_line=True)
        to_unpack = self._load_data(self.train_dir, ignore_empty_files=True)
        c.millis()

        self.files_sentences_words[TRAIN], self.list_domains, self.id_file2domain[TRAIN], self.files_ids[TRAIN], \
        self.files_names[TRAIN] = to_unpack

        for domain in self.list_domains:
            self.name_domain2token_domain[domain] = tokenizer.tokenize_line(domain, pattern="[\W_]")
        # return to_unpack

    def load_dev_data(self):
        """
        Reads the dev data
        """
        c = Chrono("Loading dev data...", new_line=True)
        to_unpack = self._load_data(self.dev_dir)
        c.millis()

        self.files_sentences_words[DEV], _, self.id_file2domain[DEV], self.files_ids[DEV], self.files_names[
            DEV] = to_unpack
        # return to_unpack

    def load_test_data(self):
        """
        Reads the test data
        """
        c = Chrono("Loading test data...", new_line=True)

        data = []
        file_ids = []
        file_names = []
        file_id = 0

        for f in tqdm.tqdm(os.listdir(self.test_dir)):
            if f.endswith(".txt"):
                lines = DataManager._parse_file(os.path.join(self.test_dir, f), True)
                if lines:
                    data.append(lines)
                else:
                    data.append([["UNK"]])
                    print("\nWARNING: empty file ", f, " in ", self.test_dir, "\n")

                file_ids.append(file_id)
                file_names.append(f)

                file_id += 1

        self.files_sentences_words[TEST] = data
        self.files_ids[TEST] = file_ids
        self.files_names[TEST] = file_names

        self.id_file2domain[TEST] = None
        c.millis()

    @staticmethod
    def _load_data(directory, ignore_empty_files=False):
        """
        Read that data from directory, remembering the file name and the domain to which it belongs to.
        It generates a cache file for fast reuse.

        :param directory: the data directory
        :param ignore_empty_files: True if empty files must be ignored

        :returns data: a list (files) of list (sentences) of words
        :returns list_domains: a list of domains encountered
        :returns idfile2domain: a dictionary that maps files to domani
        :returns file_ids: a list of file ids
        :returns file_names: a list of file names
        """
        data = []
        file_ids = []
        file_names = []

        id_file2domain = {}
        list_domains = []
        file_id = 0

        for domain in os.listdir(directory):
            list_domains.append(domain)

            listdir = os.listdir(os.path.join(directory, domain))
            for f in tqdm.tqdm(listdir, total=len(listdir)):
                if f.endswith(".txt"):

                    lines = DataManager._parse_file(os.path.join(directory, domain, f), True)
                    if lines:  # let out empty files, or files composed only by special chars!
                        data.append(lines)

                        file_ids.append(file_id)
                        file_names.append(f)

                        id_file2domain[file_id] = domain
                        file_id += 1

                    else:
                        print("\nWARNING: empty file ", f, " in ", directory, domain, "\n")

                        if not ignore_empty_files:
                            data.append([["UNK"]])

                            file_ids.append(file_id)
                            file_names.append(f)

                            id_file2domain[file_id] = domain
                            file_id += 1

        return data, list_domains, id_file2domain, file_ids, file_names

    @staticmethod
    def _parse_file(filename, additional_stopwords):
        """
        Reads a file and parse it into a list of sentences

        :param filename: the file to read
        :param additional_stopwords: a list of stopwords to uso, in combination to the default ones
        :return: a list of lines
        """
        lines = []
        with open(filename) as file:
            for line in file.readlines():
                splitted_line = tokenizer.tokenize_line(line, use_long_stopwords=additional_stopwords)
                if splitted_line:  # let out empty lines!!!
                    lines.append(splitted_line)
        return lines


class Metrics:
    """
    Manages the use of the embeddings and performs metric calculations on them
    """

    def __init__(self):
        self.dm = DataManager.get_instance()
        # free some memory, actually do not need them
        del self.dm.id_file2setwords
        del self.dm.word2total_count

        # ------------  THE THRESHOLD ------------ #
        # Which words should I consider similar?
        # I consider similar words that are as distant at least as the words "crime" and "victim"
        reference_distance = numpy.linalg.norm(self.dm.word2vec["crime"] - self.dm.word2vec["victim"])
        self.threshold = 1 / (1 + reference_distance)

        # ----- EACH DICTIONARY IS DIVIDED FIRSTLY BY TRAIN, DEV OR TEST ----- #
        self.id_file2vec = {}
        self.id_file2embedding = {}
        self.id_file2domain2domain_importance = {}

        # ----> Computes the domain embedding!
        # At each domain is associated an embedding, considering the domain name as a list of word and taking the centroid
        self.domain2embedding = {
            domain: numpy.average([self.dm.lookup(t) for t in self.dm.name_domain2token_domain[domain]], axis=0) for
            domain in self.dm.list_domains}
        # -------------------------------------------------------------------- #

        self.compute_files_embedding()
        self.compute_files_domain_importance()
        self.compute_file2vec()

    @staticmethod
    def cos_sim(vec1, vec2):
        """
        Compute the cosine similarity (not efficient for large computations)

        :param vec1: the first 1-dimensional vector
        :param vec2: the second 1-dimensional vector
        :return: the computed cosine similarity
        """
        return 1 - scipy.spatial.distance.cosine(vec1, vec2)

    def compute_file2vec(self):
        """
        For each file, it computes the correspondent vector.
        For test purposes at each file is associated:
        ) The centroid of all the words embeddings in it
        ) The concatenation of the importance vector (see compute_files_domain_importance) and similarity vector,
            that is, the cosin similarity between the centroid of the file and the centroid of the domains name
        ) The concatenation of the previous two.
        """
        for dataset in [TRAIN, DEV, TEST]:
            self.id_file2vec[dataset] = {x: [] for x in [ONLY_EMBD, ONLY_IMPCOS, BOTH]}

            for file in tqdm.tqdm(range(len(self.dm.files_ids[dataset]))):
                embed = self.id_file2embedding[dataset][file]
                imp = [self.id_file2domain2domain_importance[dataset][file][domain] for domain in self.dm.list_domains]
                cos = [self.cos_sim(self.id_file2embedding[dataset][file], self.domain2embedding[domain]) for domain in
                       self.dm.list_domains]

                self.id_file2vec[dataset][ONLY_EMBD].append(embed)
                self.id_file2vec[dataset][ONLY_IMPCOS].append(numpy.concatenate((imp, cos)))
                self.id_file2vec[dataset][BOTH].append(numpy.concatenate((embed, imp, cos)))

    def compute_files_embedding(self):
        """
        For each file, computes the centroid of all the words in it.
        If a file is empty, it is considered equal to the embedding of UNK
        """
        c = Chrono("Computing file embeddings...", new_line=True)

        cache = Utils.load_pickle(FILE_EMBEDDINGS_CACHE)
        if cache:
            c.millis("loaded cached")
            self.id_file2embedding = cache
            return

        for dataset in [DEV, TEST, TRAIN]:
            self.id_file2embedding[dataset] = {}
            for i, file in tqdm.tqdm(enumerate(self.dm.files_sentences_words[dataset])):
                a = [self.dm.lookup(word) for sentence in file for word in sentence]
                self.id_file2embedding[dataset][i] = numpy.average(a, axis=0) if a else self.dm.word2vec["UNK"]
            del self.dm.files_sentences_words[dataset]
        del self.dm.files_sentences_words

        Utils.save_pickle(self.id_file2embedding, FILE_EMBEDDINGS_CACHE)
        c.millis()

    def compute_files_domain_importance(self):
        """
        Computes a value "importance" for each pair file-domain.
        It is computed as follows:

        (remember that in the __init__ we calculated the embedding for each domain name)

        Given a file f, and the current domain d, the importance of the domain d for the file f is given by the formula:

            sum_{word in f | the similarity between the word and embedding(d) is at least the threshold}
                    {
                        similarity(word, embedding(d) *
                        number of occurences of word in f *
                        log inverse document frequency of word  ( number of docs / number of docs in which word appears)
                    }

        So, for each file are created 'number of domains' values of importance.
        """
        c = Chrono("Computing domain importance...")

        caching_distance = {}
        file2wc = self.dm.id_file2word2number_words

        for dataset in [TRAIN, DEV, TEST]:
            cache_file = dataset + DOMAIN_IMPORTANCE_CACHE_FILE

            cache = Utils.load_pickle(cache_file)
            if cache:
                c.millis("loaded cache")
                self.id_file2domain2domain_importance[dataset] = cache
                continue

            self.id_file2domain2domain_importance[dataset] = {}

            for file in tqdm.tqdm(self.dm.files_ids[dataset], total=len(self.dm.files_ids[dataset])):
                self.id_file2domain2domain_importance[dataset][file] = Counter()

                for domain in self.dm.name_domain2token_domain:
                    for word in file2wc[dataset][file]:
                        if (domain, word) in caching_distance:
                            current_importance = caching_distance[(domain, word)]
                        else:
                            current_importance = numpy.linalg.norm(self.domain2embedding[domain] - self.dm.lookup(word))
                            current_importance = 1 / (1 + current_importance)
                            caching_distance[(domain, word)] = current_importance

                        if current_importance >= self.threshold:
                            self.id_file2domain2domain_importance[dataset][file][domain] += (
                                    current_importance *
                                    file2wc[dataset][file][word] *
                                    (1 + math.log(
                                        len(self.dm.files_names[TRAIN]) / self.dm.word2doc_frequency[TRAIN][word], 10)))

            Utils.save_pickle(self.id_file2domain2domain_importance[dataset], cache_file)
        c.millis()


class Classifier_utils:
    """
    The class that provides the x_train and y_train
    """

    @staticmethod
    def get_train_and_test_data():
        """
        Computer the x_train and y_train for the TRAIN, DEV and TEST.
        Uses a pickle cache to speedup consecutive runs.
        """
        data = Utils.load_pickle(TRAINING_DATA_CACHE)
        if data:
            assert len(data[X_TRAIN][ONLY_EMBD][0]) == len(data[X_DEV][ONLY_EMBD][0]) == len(
                data[X_TEST][ONLY_EMBD][0]) == 128
            assert len(data[X_TRAIN][ONLY_IMPCOS][0]) == len(data[X_DEV][ONLY_IMPCOS][0]) == len(
                data[X_TEST][ONLY_IMPCOS][0]) == 34 + 34
            assert len(data[X_TRAIN][BOTH][0]) == len(data[X_DEV][BOTH][0]) == len(
                data[X_TEST][BOTH][0]) == 128 + 34 + 34

            assert len(data[X_TRAIN][ONLY_EMBD]) == len(data[X_TRAIN][ONLY_IMPCOS]) == len(data[X_TRAIN][BOTH]) == len(
                data[Y_TRAIN])
            assert len(data[X_DEV][ONLY_EMBD]) == len(data[X_DEV][ONLY_IMPCOS]) == len(data[X_DEV][BOTH]) == len(
                data[Y_DEV])
            assert len(data[X_TEST][ONLY_EMBD]) == len(data[X_TEST][ONLY_IMPCOS]) == len(data[X_TEST][BOTH]) == 24526
            return data

        metrics = Metrics()

        data = {
            X_TRAIN: {x: metrics.id_file2vec[TRAIN][x] for x in [ONLY_EMBD, ONLY_IMPCOS, BOTH]},
            Y_TRAIN: [metrics.dm.id_file2domain[TRAIN][file] for file in metrics.dm.files_ids[TRAIN]],

            X_DEV: {x: metrics.id_file2vec[DEV][x] for x in [ONLY_EMBD, ONLY_IMPCOS, BOTH]},
            Y_DEV: [metrics.dm.id_file2domain[DEV][file] for file in metrics.dm.files_ids[DEV]],

            X_TEST: {x: metrics.id_file2vec[TEST][x] for x in [ONLY_EMBD, ONLY_IMPCOS, BOTH]},

            TRAIN_FILES_IDS: metrics.dm.files_ids[TRAIN],
            DEV_FILES_IDS: metrics.dm.files_ids[DEV],
            TEST_FILES_IDS: metrics.dm.files_ids[TEST],

            TRAIN_FILENAMES: metrics.dm.files_names[TRAIN],
            DEV_FILENAMES: metrics.dm.files_names[DEV],
            TEST_FILENAMES: metrics.dm.files_names[TEST],

            TRAIN_IDFILE_DOMAIN: metrics.dm.id_file2domain[TRAIN],
            DEV_IDFILE_DOMAIN: metrics.dm.id_file2domain[DEV],
            TEST_IDFILE_DOMAIN: metrics.dm.id_file2domain[TEST],

        }

        assert len(data[X_TRAIN][ONLY_EMBD][0]) == len(data[X_DEV][ONLY_EMBD][0]) == len(
            data[X_TEST][ONLY_EMBD][0]) == 128
        assert len(data[X_TRAIN][ONLY_IMPCOS][0]) == len(data[X_DEV][ONLY_IMPCOS][0]) == len(
            data[X_TEST][ONLY_IMPCOS][0]) == 34 + 34
        assert len(data[X_TRAIN][BOTH][0]) == len(data[X_DEV][BOTH][0]) == len(data[X_TEST][BOTH][0]) == 128 + 34 + 34

        assert len(data[X_TRAIN][ONLY_EMBD]) == len(data[X_TRAIN][ONLY_IMPCOS]) == len(data[X_TRAIN][BOTH]) == len(
            data[Y_TRAIN])
        assert len(data[X_DEV][ONLY_EMBD]) == len(data[X_DEV][ONLY_IMPCOS]) == len(data[X_DEV][BOTH]) == len(
            data[Y_DEV])
        assert len(data[X_TEST][ONLY_EMBD]) == len(data[X_TEST][ONLY_IMPCOS]) == len(data[X_TEST][BOTH]) == 24526

        Utils.save_pickle(data, TRAINING_DATA_CACHE)
        return data

    @staticmethod
    def classify_based_only_on_domain_name():
        """
        Performs a classification on the DEV where the only information used
        that comes from TRAIN is the list of domain name.

        It uses the most important domain as the predicted class
        """
        print("CLASSIFICATION WITHOUT TRAINING")
        metrics = Metrics()

        unk_counter = 0

        predicted = []
        true = [metrics.dm.id_file2domain[DEV][file] for file in metrics.dm.files_ids[DEV]]

        for file in tqdm.tqdm(metrics.dm.files_ids[DEV]):
            if metrics.id_file2domain2domain_importance[DEV][file]:
                file_classification = metrics.id_file2domain2domain_importance[DEV][file].most_common(1)[0][0]
                predicted.append(file_classification)
            else:
                predicted.append(random.sample(metrics.dm.list_domains, 1)[0])
                unk_counter += 1

        print(sklearn.metrics.classification_report(true, predicted))


# def most_common(lst):
#     counter = Counter(lst)
#     return max(lst, key=counter.get)
#
#
# def majority_vote(list_of_list):
#     return [most_common(x) for x in zip(*list_of_list)]


import time
# Performs the training a saves the results.
if __name__ == "__main__":
    time_id = time.strftime("%d-%m-%Y_%Hh-%Mm")
    run_id = "result_training_summary_{}".format(time_id)
    c = Chrono("Training {}...".format(run_id), new_line=True)

    result_summary = {"id": run_id}


    # Get the train data

    data = Classifier_utils.get_train_and_test_data()

    type_of_file_vector = [ONLY_EMBD, ONLY_IMPCOS, BOTH]
    x_train = {x: numpy.array(data[X_TRAIN][x]) for x in type_of_file_vector}
    y_train = numpy.array(data[Y_TRAIN])

    x_dev = {x: numpy.array(data[X_DEV][x]) for x in type_of_file_vector}
    y_dev = numpy.array(data[Y_DEV])

    x_test = {x: numpy.array(data[X_TEST][x]) for x in type_of_file_vector}

    x_train = x_train[BOTH]
    x_dev = x_dev[BOTH]
    x_test = x_test[BOTH]

    # -----------------------------------------------------------------------------------------------
    # Train a XGBoost classifier.
    # The tuning of the parameters has been made by hand, looking at the loss during the training.
    # A grid search woulg have required too much time.

    classifier = xgboost.XGBClassifier(n_estimators=10000,
                                       learning_rate=0.01,
                                       objective="multi:softmax",
                                       min_child_weight=5,
                                       max_depth=30,
                                       gamma=0,
                                       colsample_bytree=0.6,
                                       colsample_bylevel=1,
                                       subsample=1,
                                       silent=True,
                                       nthread=12
                                       )
    result_summary["classifier_params"] = classifier.get_params()
    print(json.dumps(classifier.get_params(), indent=4))

    classifier.fit(x_train, y_train, early_stopping_rounds=500, eval_metric=["merror"], eval_set=[(x_dev, y_dev)],
                   verbose=True)
    c.millis()
    Utils.save_pickle(classifier, TRAINED_MODEL)

    # -----------------------------------------------------------------------------------------------

    prediction_dev = classifier.predict(x_dev)
    prediction_test = classifier.predict(x_test)

    result_summary["y_true"] = y_dev
    result_summary["y_pred"] = prediction_dev
    result_summary["test_predictions"] = prediction_test

    result_summary["dev_filenames"] = data[DEV_FILENAMES]
    result_summary["test_filenames"] = data[TEST_FILENAMES]

    precision, recall, fmeasure, support = sklearn.metrics.precision_recall_fscore_support(y_dev, prediction_dev)
    result_summary["classes"] = classifier.classes_
    result_summary["precision"] = precision
    result_summary["recall"] = recall
    result_summary["f-measure"] = fmeasure
    result_summary["support"] = support

    precision_avg, recall_avg, fmeasure_avg, _ = sklearn.metrics.precision_recall_fscore_support(y_dev, prediction_dev,
                                                                                                 average="weighted")
    result_summary["precision_average"] = precision_avg
    result_summary["recall_average"] = recall_avg
    result_summary["f-measure_average"] = fmeasure_avg

    classification_report = sklearn.metrics.classification_report(y_dev, prediction_dev)
    result_summary["classification_report"] = classification_report
    print(classification_report)

    confusion_matrix = sklearn.metrics.confusion_matrix(y_dev, prediction_dev)
    result_summary["confusion_matrix"] = confusion_matrix

    output_file = time_id + "_test_answers.tsv"
    str_file = ""
    for i, pred in enumerate(prediction_test):
        file_name, _ = os.path.splitext(data[TEST_FILENAMES][i])
        str_file += "{}\t{}\n".format(file_name, pred)

    with open(output_file, "w") as handle:
        handle.write(str_file)

    Utils.save_pickle(result_summary, run_id + ".pickle")

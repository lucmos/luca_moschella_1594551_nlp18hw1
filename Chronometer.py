import time


class Chrono:
    """
    A class that implements a simple timer for the elapsed time, measured in millis.
    Useful even for only printing the current status: Initializing... done
    """
    def __init__(self, initial_message, new_line=False, final_message="done"):
        """
        Creates the chronometer and starts it

        :param initial_message: initial message to print
        :param new_line: True if the print must include a new line
        :param final_message: message to print end called by end
        """
        self.initial_message = initial_message
        self.final_message = final_message
        self.current_milli_time = lambda: int(round(time.time() * 1000))
        self.start_time = self.current_milli_time()

        print(initial_message, end="\n" if new_line else "", flush=True)

    def millis(self, message=None):
        """
        Print the final message. Print other optional information and checks the elapsed time

        :param message: additional information whether to print a message or not
        :return: the elapsed time
        """
        elapsed = self.current_milli_time() - self.start_time
        if message:
            print("\t{}, {} (in {} millis)".format(self.final_message, message, elapsed))
        else:
            print("\t{} (in {} millis)".format(self.final_message, elapsed))
        return elapsed

import os
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib
import matplotlib.pyplot as plt

import Utils


DOMAIN_SUMMARY_FILE = "domain_output/result_training_summary_15-04-2018_10h-56m.pickle"

OUTPUT1 = "output1/summary_14-04-2018_21h-35m.pickle"
OUTPUT2 = "output2/summary_14-04-2018_22h-29m.pickle"
OUTPUT3 = "output3/summary_14-04-2018_23h-39m.pickle"
OUTPUT4 = "output4/summary_15-04-2018_00h-16m.pickle"

OUTPUT_FINAL = "output_finale/summary_12-04-2018_07h-15m.pickle"

PICS_FOLDER = "pics/"
params = {'legend.fontsize': 'x-large',
          'figure.figsize': (15, 5),
         'axes.labelsize': 'x-large',
         'axes.titlesize':'x-large',
         'xtick.labelsize':'x-large',
         'ytick.labelsize':'x-large'}
plt.rcParams.update(params)

CMAP = sns.light_palette((210, 90, 60), n_colors=64, input="husl")


def plot_confusion_matrix(labels, confusion_matrix):
    """
    Plots the confusion matrix using a seaborn heatmap
    :param labels: labels of the confusion matrix
    :param confusion_matrix: the confusion of matrix, in a list of list format
    """
    sns.set(style="white")
    data = pd.DataFrame(confusion_matrix, index=[i for i in range(len(labels))], columns=[i for i in range(len(labels))])

    f, ax = plt.subplots(figsize=(20,20))
    annot = data
    data = np.where(data != 0, np.log(data), 0)
    heatmap = sns.heatmap(data, xticklabels=labels, cbar=False, yticklabels=labels, cmap=CMAP,
                square=True, annot=annot, fmt="g", linewidths=1, annot_kws={"size": 11})  # font size
    heatmap.yaxis.set_ticklabels(heatmap.yaxis.get_ticklabels(), rotation=0, ha='right', fontsize=12)
    heatmap.xaxis.set_ticklabels(heatmap.xaxis.get_ticklabels(), rotation=45, ha='right', fontsize=12)
    plt.title("CONFUSION MATRIX")
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.tight_layout()
    plt.savefig(os.path.join(PICS_FOLDER, "confusion_matrix.png"), dpi=500)
    plt.close(f)


def plot_prfs_matrix(label, precision, recall, fmeasure, suppport, pavg, ravg, favg):
    """
    Plots the precision, recall, f-measure and support for each class and the weighted average.

    :param label:  the labels
    :param precision: list of precisions
    :param recall: list of recalls
    :param fmeasure: list of fmeasures
    :param suppport: list of supports
    :param pavg: the averaged precision
    :param ravg: the averaged recall
    :param favg: the averaged fmeasure
    """
    sns.set(style="white")
    data = pd.DataFrame({"Precision": precision, "Recall":recall, "F-measure":fmeasure, "Support":suppport}, index=label, columns=["Precision", "Recall", "F-measure", "Support"])
    avg = pd.DataFrame({"Precision": pavg, "Recall":ravg, "F-measure":favg}, index=["AVERAGE"], columns=["Precision", "Recall", "F-measure", "Support"])

    f, ax = plt.subplots(figsize=(13, 20))

    data["Support"] = data["Support"] / sum(data["Support"])
    data = data.append(avg)
    annot = data
    data = data.apply(lambda x: Utils.min_max_normalization(x), axis=0)

    sns.heatmap(data, cbar=False, cmap=CMAP, annot=annot, fmt=".1%", linewidths=2.5, annot_kws={"size": 16})
    ax.xaxis.tick_top()
    plt.tight_layout()
    plt.savefig(os.path.join(PICS_FOLDER, "precision_recall.png"), dpi=500)
    plt.close(f)

def plot(steps, values, windows, legend_pos, label_pattern, filename, title, y_label, x_label, dinamic_window_display=True, multiple_labels=None):
    """
    Plots n lines into a single plot

    :param steps: a list of values for the x-axis
    :param values: a list of values for the y-axis
    :param windows: determine which window size is used by each line
    :param legend_pos: the position of the legend
    :param label_pattern: a single label that can be parametrized by a single value
    :param filename: the file name
    :param title: the title
    :param y_label: the y-label
    :param x_label: the x-label
    :param dinamic_window_display: True if the the label_pattern must include the window size
    :param multiple_labels: the label to use for each line
    :return:
    """
    plt.style.use("ggplot")

    colors = ['darkorange', 'navy', "darkgreen", "tomato"]
    fig = plt.figure()
    for i in range(len(steps)):
        curr = multiple_labels[i] if multiple_labels else label_pattern.format(windows[i]) if dinamic_window_display else label_pattern
        plt.plot(steps[i], values[i], color=colors[i], lw=1.5, label=curr)

    plt.legend(loc=legend_pos)
    plt.xlabel(x_label)
    if y_label:
        plt.ylabel(y_label)
    plt.title(title)

    plt.tight_layout()
    plt.savefig(os.path.join(PICS_FOLDER, filename), dpi=500)
    plt.close(fig)


def plot_window(distr, size):
    """
    Plots the window probability distribution

    :param distr: the distribution of probability
    :param size: the size of the window
    :return:
    """
    plt.style.use("ggplot")
    distr[size] = 0
    sns.barplot(list(i - size for i in range(len(distr))), distr)
    plt.xlabel("Position in window")
    plt.ylabel("Probability")
    plt.title("Window probability distribution")
    plt.tight_layout()
    plt.savefig(os.path.join(PICS_FOLDER, "window_distribution.png"), dpi=500)
    plt.show()


if __name__ == '__main__':
    """
    Reads the results and makes the plot.
    """

    data = Utils.load_pickle(DOMAIN_SUMMARY_FILE)
    for k in data:
        print(k)

    o = Utils.load_pickle(OUTPUT_FINAL)
    print()
    for i in o:
        print(i)

    plot_window(o["window_distribution:"], o["window_size"])
    print(o["window_distribution:"])

    steps, avgloss, _, _, acc = zip(*o["step_avgloss_correct_total_accuracy"])
    plot([steps], [avgloss], None, "upper right", "loss ({0:0.3f})".format(avgloss[-1]), "final_loss.png", "Loss",
         "Loss", "Steps", dinamic_window_display=False)
    plot([steps], [acc], None, "upper left", "accuracy ({0:0.3f})".format(acc[-1]), "final_acc.png", "Accuracy",
         "Accuracy", "Steps", dinamic_window_display=False)
    plot([steps, steps], [acc, avgloss], None, "upper left", None, "final_acc_loss.png", "Accuracy - Loss", None,
         "Steps", dinamic_window_display=False,
         multiple_labels=["accuracy ({0:0.3f})".format(acc[-1]), "loss ({0:0.3f})".format(avgloss[-1])])

    o1 = Utils.load_pickle(OUTPUT1)
    o2 = Utils.load_pickle(OUTPUT2)
    o3 = Utils.load_pickle(OUTPUT3)
    o4 = Utils.load_pickle(OUTPUT4)

    steps_all = []
    losses_all = []
    accuracy_all = []
    window_sizes = []
    for k in [o1, o2, o3, o4]:
        steps, avgloss, _, total, acc = zip(*k["step_avgloss_correct_total_accuracy"])
        steps_all.append(steps)
        losses_all.append(avgloss)
        accuracy_all.append(acc)
        window_sizes.append(k["window_size"])

    plot(steps_all, accuracy_all, window_sizes, "upper left", "window size {}", "accuracy_comparison.png", "Accuracy comparison", "Accuracy", "Steps")
    plot(steps_all, losses_all, window_sizes, "upper right", "window size {}", "loss_comparison.png", "Loss comparison", "Loss", "Steps")

    plot_confusion_matrix(data["classes"], data["confusion_matrix"])
    plot_prfs_matrix(data["classes"], data["precision"], data["recall"], data["f-measure"], data["support"], data["precision_average"], data["recall_average"], data["f-measure_average"])

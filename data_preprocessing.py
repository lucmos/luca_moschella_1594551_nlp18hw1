import collections

import numpy as np
import random
import Utils


def generate_batch(batch_size, curr_sentence, curr_word, curr_label, window_size, window_prob_distr, data):
    """
    This function generates the train data and label batch from the dataset.
    It start generating the batch from the given indexes, so it is possible to generate the next batch exactly.

    :param batch_size: size of the batch that must be generated
    :param curr_sentence: index of the current sentence that's being considered
    :param curr_word: index of the current word that's being considered
    :param curr_label: index of the current label, the context, that's being considered
    :param window_size: half the size of the window in which it takes labels for the current word
    :param window_prob_distr: an array that describes the probabilities along the window.
                            In position i there is the probability with which I should generate the couple (word, label_i)
    :param data: the data, a list of sentences.

    :return return x_train: the list of (target word), the x_train.
    :return y_train: the list of labels, the y_train.
    :return curr_sentence: the next index of the sentence to use to generate the next batch
    :return curr_word: the next index of the word to use to generate the next word
    :return curr_label: the next index of the label to use to generate the next label, for the current word
    """
    x_train = []
    y_train = []

    # Until I generate a sufficient number of train data
    while len(x_train) < batch_size:
        sentence = data[curr_sentence]
        label_index_in_window = curr_label - curr_word

        # With probability according to the distribution, try to generate the pair
        if random.random() < window_prob_distr[label_index_in_window + window_size]:
            target = sentence[curr_word]
            label = sentence[curr_label]

            # Add the pair if it is valid
            if target != label:
                x_train.append(target)
                y_train.append([label])

        # Prepare for the next possible pair
        curr_label += 1

        # skip the iteration with itself
        if curr_label == curr_word:
            curr_label += 1

        # If completed the current word or arrived to end of the sentence go to the next word
        if curr_label == curr_word + window_size + 1 or curr_label == len(sentence):
            curr_word += 1
            curr_label = max(0, curr_word - window_size)

        # If completed the current sentence go to the next one
        if curr_word == len(data[curr_sentence]):
            curr_label = 0
            curr_word = 0
            curr_sentence += 1

        # If used all the data, restart
        if curr_sentence == len(data):
            print("\nAll the data has been used! Restarting the batching!\n\n")
            curr_label = 0
            curr_word = 0
            curr_sentence = 0
    return x_train, y_train, curr_sentence, curr_word, curr_label


def build_dataset(words_in_contex, vocab_size):
    """
    This function is responsible of generating the dataset and dictionaries.
    While constructing the dictionary take into account the unseen words by
    retaining the rare (less frequent) words of the dataset from the dictionary
    and assigning to them a special token in the dictionary: UNK. This
    will train the model to handle the unseen words.

    :param words_in_contex: the list of sentences to convert
    :param vocab_size: the size of the vocabolary

    :return:
    :return data: list of codes (integers from 0 to vocabulary_size-1).
                This is the original text but words are replaced by their codes
    :return dictionary: map of words(strings) to their codes(integers)
    :return reverse_dictionary: maps codes(integers) to words(strings)
    """
    # I count the occurrences of each word in the text
    counter = collections.Counter()
    for contex in words_in_contex:
        for word in contex:
            counter[word] += 1
    counter_len = len(counter)
    print("Number of distinct words: {}".format(counter_len))

    # I build the dictionary taking the vocab_size - 1 most common elements, and adding an UNK element for the others
    dictionary = {key: index for index, (key, _) in enumerate(counter.most_common(vocab_size - 1))}
    assert "UNK" not in dictionary
    dictionary["UNK"] = vocab_size - 1

    # I print some information
    print("Dictionary size: {} (excluded {} words)".format(vocab_size, counter_len - vocab_size))
    dict_counts = {x: counter[x] for x in dictionary if x is not "UNK"}
    occ_dict = sum(dict_counts[x] for x in dict_counts)
    print("Total occurrencies of words in dictionary: {}".format(occ_dict))
    print("Total occurrencies of ignored words: {}".format(sum(counter[x] for x in counter) - occ_dict))
    less_freq_word = min(dict_counts, key=counter.get)
    print("Less frequent word in dictionary appears {} times ({})".format(dict_counts[less_freq_word], less_freq_word))

    inverted_dictionary = {value: key for key, value in dictionary.items()}
    data = [[dictionary[i] if i in dictionary else dictionary["UNK"] for i in contex] for contex in words_in_contex]

    return data, dictionary, inverted_dictionary


def save_vectors(vectors, inverted_dict, filename):
    """
    Save the normalized embeddings. The emeddings are saved as a dictionary: {word: word_embedding} in a pickle format.

    :param vectors: embeddings to save
    :param inverted_dict: dictionary that maps indexes to words
    :param filename: filename of the pickle file
    """
    emb_dict = {}
    for i, embeddings in enumerate(vectors):
        emb_dict[inverted_dict[i]] = embeddings
    Utils.save_pickle(emb_dict, filename)


# Reads through the analogy question file.
#    Returns:
#      questions: a [n, 4] numpy array containing the analogy question's
#                 word ids.
#      questions_skipped: questions skipped due to unknown words.
#
def read_analogies(file, dictionary):
    questions = []
    questions_skipped = 0
    with open(file, "r") as analogy_f:
        for line in analogy_f:
            if line.startswith(":"):  # Skip comments.
                continue
            words = line.strip().lower().split(" ")
            ids = [dictionary.get(str(w.strip())) for w in words]
            if None in ids or len(ids) != 4:
                questions_skipped += 1
            else:
                questions.append(np.array(ids))
    print("Eval analogy file: ", file)
    print("Questions: ", len(questions))
    print("Skipped: ", questions_skipped)
    return np.array(questions, dtype=np.int32)
